<?php
require('../fpdf.php');

class PDF extends FPDF
{
	// Cabecera de p�gina
	function Header()
	{
		//-------logo y fecha alta
		// Logo de la empresa
		$this->Image('logo.png',10,15,80);
		// Arial bold 15
		$this->SetFont('Arial','',12);
		// Movernos a la derecha
		$this->Cell(80);
		// FECHA DE alta
		$this->Cell(150,30,'Lima, 10 de setiembre del 2018',0,0,'C');
		// Salto de l�nea
		$this->Ln(25);


		//titulo general
		// Arial bold 15
		$this->SetFont('Arial','b',22);
		// Movernos a la derecha
		$this->Cell(85);
		// FECHA DE alta
	 $this->Cell(20,30,'CARTA DE PRESENTACI�N',0,1,'C');
	
		// Salto de l�nea
		
		$this->Line(20, 62, 210-20, 62); // 20mm from each edge
	  $this->Line(50, 62, 210-50, 62); // 50mm from each edge
	  
	  $this->SetDrawColor(188,188,188);
		$this->Line(20,63,210-20,63);
		$this->Ln(13);
	}
}



// Creaci�n del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','B',14);
// estacioon
$pdf->Cell(9);
$pdf->Cell(0,0,'E/S TAVIRSA:',0,1,'L');
$pdf->Ln(13);

//TEXTO
$pdf->Cell(9);
$pdf->SetFont('Arial','',14);

$pdf->Cell(0,0,'T-Soluciona tiene el agrado de presentar al Sr.:',0,1,'L');

//NOMBRE DEL POSTULANTE
$pdf->Ln(13);
$pdf->Cell(80);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(20,5,'LA TORRE GUERRERO, JORGE LUIS',0,1,'C');

$pdf->Ln(13);
$pdf->Cell(9);
$pdf->SetFont('Arial','',14);
$pdf->Cell(0,0,'Quien ha ido evaluado a trav�s de un proceso de filtros, para posici�n de:',0,1,'L');

//CARGO
$pdf->Ln(13);
$pdf->Cell(80);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(20,5,'VENDEDOR PLAYA',0,1,'C');

//direccion y estacion
$pdf->Ln(13);
$pdf->Cell(9);
$pdf->SetFont('Arial','',14);
$pdf->MultiCell(151,5,'Direcci�n: Av. Canada Cra.11 con Victor Alzamora - LA VICTORIA / E/S CANADA',0,'C',false);

//TEXTO
$pdf->Ln(13);
$pdf->Cell(9);
$pdf->MultiCell(170,5,'De esta forma, la persona en cuesti�n culmin� satisfactoriament el paso por nuestra consultora y es considerada APTA.',0,'J',false);
$pdf->Ln(5);
$pdf->Cell(9);
$pdf->Cell(0,0,'Sin otro particular, agradecemos su preferencia.',0,1,'L');
$pdf->Ln(10);
$pdf->Cell(9);
$pdf->Cell(0,0,'Atte.',0,1,'L');

//FIRMA 
$pdf->Ln(10);
$pdf->Image('FIRMA CARMEN.png',72,217,75);
$pdf->Line(63,255,210-63,255);
$pdf->Ln(50);
$pdf->Cell(54);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(0,0,'CARMEN PALACIOS CHAFALOTE',0,1,'L');
$pdf->Ln(5);
$pdf->Cell(56);
$pdf->SetFont('Arial','',14);
$pdf->Cell(0,0,'Selecci�n de Personal T-Soluciona','C',1);

$pdf->Output();
?>
